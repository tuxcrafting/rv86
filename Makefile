asm_src = src/asm/rv86.asm \
	$(shell find src/asm -name '*.asm' -type f)

c_src = \
	src/rom/main.c

all: rv86.img

rv86.img: rv86.bin
	cat $^ /dev/zero | dd iflag=fullblock bs=512 count=2880 of=$@

rv86.bin: $(asm_src) rv86_rom.bin
	fasm $< $@

rv86_rom.elf: $(c_src)
	riscv64-linux-gnu-gcc \
	-march=rv32i -mabi=ilp32 -mcmodel=medlow \
	-ffreestanding -nostdlib \
	-Wl,-Ttext=0x00020000 \
	-static -Os \
	-o $@ $^

rv86_rom.bin: rv86_rom.elf
	riscv64-linux-gnu-objcopy \
	-O binary -R '.note.*' \
	$< $@

run: rv86.img
	qemu-system-i386 \
	-m 16M -cpu base -enable-kvm \
	-drive if=floppy,file=rv86.img,format=raw -boot a

clean:
	rm -f *.img *.bin *.elf
