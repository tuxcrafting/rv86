base_op_imm:
	call decode_i
.pd:
	jmp word [op_imm_tbl + bx]

	align 2
op_imm_tbl:
	dw op_imm_addi
	dw op_imm_slli
	dw op_imm_slti
	dw op_imm_sltiu
	dw op_imm_xori
	dw op_imm_srli_srai
	dw op_imm_ori
	dw op_imm_andi

op_imm_addi:
	add ax, [si + 0]
	adc dx, [si + 2]
	mov [di + 0], ax
	mov [di + 2], dx
	ret

op_imm_slli:
	mov cx, ax
	and cx, 31
	mov ax, [si + 0]
	mov dx, [si + 2]
	call shift_left
	mov [di + 0], ax
	mov [di + 2], dx
	ret

op_imm_slti:
	xor bp, bp
	mov bx, [si + 0]
	mov cx, [si + 2]
	sub bx, ax
	sbb cx, dx
	jge .ge
	mov bp, 1
.ge:
	xor ax, ax
	mov [di + 0], bp
	mov [di + 2], ax
	ret

op_imm_sltiu:
	xor bp, bp
	mov bx, [si + 0]
	mov cx, [si + 2]
	sub bx, ax
	sbb cx, dx
	jae .ae
	mov bp, 1
.ae:
	xor ax, ax
	mov [di + 0], bp
	mov [di + 2], ax
	ret

op_imm_xori:
	xor ax, [si + 0]
	xor dx, [si + 2]
	mov [di + 0], ax
	mov [di + 2], dx
	ret

op_imm_srli_srai:
	mov cx, ax
	and cx, 31

	test ax, 1 shl 10

	mov ax, [si + 0]
	mov dx, [si + 2]

	jnz .arithmetic

	call shift_right
	jmp .end
.arithmetic:
	call shift_right_arithmetic
.end:
	mov [di + 0], ax
	mov [di + 2], dx
	ret

op_imm_ori:
	or ax, [si + 0]
	or dx, [si + 2]
	mov [di + 0], ax
	mov [di + 2], dx
	ret

op_imm_andi:
	and ax, [si + 0]
	and dx, [si + 2]
	mov [di + 0], ax
	mov [di + 2], dx
	ret
