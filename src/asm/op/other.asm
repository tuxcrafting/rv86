base_custom:
	or dx, dx
	js .in

	mov dx, [10 * 4 + 0]
	mov al, [11 * 4 + 0]
	out dx, al
	ret
.in:
	mov dx, [10 * 4 + 0]
	xor ax, ax
	mov ax, [10 * 4 + 2]
	in al, dx
	mov ax, [10 * 4 + 0]
	ret

base_auipc:
	call decode_u
.pd:
	mov bx, [pc + 0]
	mov cx, [pc + 2]
	add bx, ax
	adc cx, dx
	mov [di + 0], bx
	mov [di + 2], cx
	ret

base_lui:
	call decode_u
.pd:
	mov [di + 0], ax
	mov [di + 2], dx
	ret

base_jalr:
	call decode_i
.pd:
	add ax, [si + 0]
	adc dx, [si + 2]

	and ax, not 1

	mov bx, [new_pc + 0]
	mov [di + 0], bx
	mov bx, [new_pc + 2]
	mov [di + 2], bx

	mov [new_pc + 0], ax
	mov [new_pc + 2], dx

	ret

base_jal:
	call decode_j
.pd:
	mov ax, [new_pc + 0]
	mov [di + 0], ax
	mov dx, [new_pc + 2]
	mov [di + 2], dx

	add bx, [pc + 0]
	adc cx, [pc + 2]
	mov [new_pc + 0], bx
	mov [new_pc + 2], cx

	ret
