base_store:
	call decode_s
.pd:
	jmp word [store_tbl + bx]

	align 2
store_tbl:
	dw store_sb
	dw store_sh
	dw store_sw
	dw error
	dw error
	dw error
	dw error
	dw error

store_sb:
	xchg si, di
	call prepare_addr
	mov al, [di + 0]
	mov [es:bx], al
	ret

store_sh:
	xchg si, di
	call prepare_addr
	mov ax, [di + 0]
	mov [es:bx], ax
	ret

store_sw:
	xchg si, di
	call prepare_addr
	mov ax, [di + 0]
	mov [es:bx], ax
	mov ax, [di + 2]
	mov [es:bx + 2], ax
	ret
