base_op:
	call decode_r
.pd:
	jmp word [op_tbl + bx]

	align 2
op_tbl:
	dw op_add_sub
	dw op_sll
	dw op_slt
	dw op_sltu
	dw op_xor
	dw op_srl_sra
	dw op_or
	dw op_and

op_add_sub:
	test cx, 32
	jnz .sub

	add ax, [si + 0]
	adc dx, [si + 2]
	mov [di + 0], ax
	mov [di + 2], dx
	ret

.sub:
	sub ax, [si + 0]
	sbb dx, [si + 2]
	mov [di + 0], ax
	mov [di + 2], dx
	ret

op_sll:
	mov cx, [si + 0]
	and cx, 31
	call shift_left
	mov [di + 0], ax
	mov [di + 2], dx
	ret

op_slt:
	sub ax, [si + 0]
	sbb dx, [si + 2]
	jge .ge
	mov bp, 1
.ge:
	xor ax, ax
	mov [di + 0], bp
	mov [di + 2], ax
	ret

op_sltu:
	sub ax, [si + 0]
	sbb dx, [si + 2]
	jae .ae
	mov bp, 1
.ae:
	xor ax, ax
	mov [di + 0], bp
	mov [di + 2], ax
	ret

op_xor:
	xor ax, [si + 0]
	xor dx, [si + 2]
	mov [di + 0], ax
	mov [di + 2], dx
	ret

op_srl_sra:
	test cx, 32
	jnz .arith
	mov cx, [si + 0]

	and cx, 31
	call shift_right
	mov [di + 0], ax
	mov [di + 2], dx
	ret

.arith:
	and cx, 31
	call shift_right_arithmetic
	mov [di + 0], ax
	mov [di + 2], dx
	ret

op_or:
	or ax, [si + 0]
	or dx, [si + 2]
	mov [di + 0], ax
	mov [di + 2], dx
	ret

op_and:
	and ax, [si + 0]
	and dx, [si + 2]
	mov [di + 0], ax
	mov [di + 2], dx
	ret
