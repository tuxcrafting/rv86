base_branch:
	call decode_b
.pd:
	jmp word [branch_tbl + bx]

	align 2
branch_tbl:
	dw branch_beq
	dw branch_bne
	dw error
	dw error
	dw branch_blt
	dw branch_bge
	dw branch_bltu
	dw branch_bgeu

branch_beq:
	mov bx, [di + 0]
	cmp bx, [si + 0]
	jnz .end
	mov bx, [di + 2]
	cmp bx, [si + 2]
	jz do_branch
.end:
	ret

branch_bne:
	mov bx, [di + 0]
	cmp bx, [si + 0]
	jnz do_branch
	mov bx, [di + 2]
	cmp bx, [si + 2]
	jnz do_branch
	ret

branch_blt:
	mov bx, [di + 0]
	mov cx, [di + 2]
	sub bx, [si + 0]
	sbb cx, [si + 2]
	jl do_branch
	ret

branch_bge:
	mov bx, [di + 0]
	mov cx, [di + 2]
	sub bx, [si + 0]
	sbb cx, [si + 2]
	jge do_branch
	ret

branch_bltu:
	mov bx, [di + 0]
	mov cx, [di + 2]
	sub bx, [si + 0]
	sbb cx, [si + 2]
	jb do_branch
	ret

branch_bgeu:
	mov bx, [di + 0]
	mov cx, [di + 2]
	sub bx, [si + 0]
	sbb cx, [si + 2]
	jae do_branch
	ret

do_branch:
	add ax, [pc + 0]
	adc dx, [pc + 2]
	mov [new_pc + 0], ax
	mov [new_pc + 2], dx
	ret
