base_load:
	call decode_i
.pd:
	jmp word [load_tbl + bx]

	align 2
load_tbl:
	dw load_lb
	dw load_lh
	dw load_lw
	dw error
	dw load_lbu
	dw load_lhu
	dw error
	dw error

load_lb:
	call prepare_addr
	xor cx, cx
	mov al, [es:bx]
	test al, 0x80
	jz .positive

	mov ah, 0xFF
	not cx
.positive:
	mov [di + 0], ax
	mov [di + 2], cx
	ret

load_lh:
	call prepare_addr
	xor cx, cx
	mov ax, [es:bx]
	test ax, 0x8000
	jz .positive

	not cx
.positive:
	mov [di + 0], ax
	mov [di + 2], cx
	ret

load_lw:
	call prepare_addr
	mov ax, [es:bx]
	mov [di + 0], ax
	mov ax, [es:bx + 2]
	mov [di + 2], ax
	ret

load_lbu:
	call prepare_addr
	xor ax, ax
	mov [di + 2], ax
	mov al, [es:bx]
	mov [di + 0], ax
	ret

load_lhu:
	call prepare_addr
	xor ax, ax
	mov [di + 2], ax
	mov ax, [es:bx]
	mov [di + 0], ax
	ret
