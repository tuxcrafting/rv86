	; shift left DX:AX by CX
shift_left:
.shift:
	or cx, cx
	jz .end
	dec cx

	shl ax, 1
	rol dx, 1
	jmp .shift
.end:
	ret

	; shift right DX:AX by CX
shift_right:
.shift:
	or cx, cx
	jz .end
	dec cx

	shr dx, 1
	ror ax, 1
	jmp .shift
.end:
	ret

	; shift right arithmetic DX:AX by CX
shift_right_arithmetic:
.shift:
	or cx, cx
	jz .end
	dec cx

	sar dx, 1
	ror ax, 1
	jmp .shift
.end:
	ret
