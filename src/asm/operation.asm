include "op/branch.asm"
include "op/load.asm"
include "op/op.asm"
include "op/op_imm.asm"
include "op/other.asm"
include "op/store.asm"

	align 2
base_opcode_map:
repeat 32
	dw error
end repeat

macro set_base_opcode opc, target {
	store word target at base_opcode_map + opc * 2
}

	set_base_opcode 00000b, base_load
	set_base_opcode 00010b, base_custom
	set_base_opcode 00100b, base_op_imm
	set_base_opcode 00101b, base_auipc
	set_base_opcode 01000b, base_store
	set_base_opcode 01100b, base_op
	set_base_opcode 01101b, base_lui
	set_base_opcode 11000b, base_branch
	set_base_opcode 11001b, base_jalr
	set_base_opcode 11011b, base_jal
