	; DI = rd addr
	; DX:AX = rs1 value
	; SI = rs2 addr
	; BX = funct3
	; CX = funct7
decode_r:
	mov di, ax
	and di, 111110000000b
	shr di, 5

	mov bx, dx
	and bx, 1111b
	shl bx, 3

	and ax, 0x8000
	shr ax, 15 - 2
	or bx, ax

	mov si, dx
	and si, 111110000b
	shr si, 2

	shr ax, 12
	and ax, 7
	mov bp, ax

	shr dx, 25
	mov cx, dx

	mov ax, [bx + 0]
	mov dx, [bx + 2]
	mov bx, bp
	ret

	; DI = rd addr
	; SI = rs1 addr
	; DX:AX = imm
	; BX = funct3 off
decode_i:
	mov di, ax
	and di, 111110000000b
	shr di, 5

	mov bx, ax
	shr bx, 11
	and bx, 1110b

	mov si, dx
	and si, 1111b
	shl si, 3

	and ax, 0x8000
	shr ax, 15 - 2
	or si, ax

	mov ax, dx
	shr ax, 4
	xor dx, dx

	test ax, 1 shl 11
	jz .positive

	or ax, 0xF000
	not dx
.positive:
	ret

	; DI = rs1 addr
	; SI = rs2 addr
	; BX = funct3 off
	; DX:AX = imm
decode_s:
	mov bx, ax
	shr bx, 11
	and bx, 1110b

	mov di, dx
	and di, 1111b
	shl di, 3

	mov cx, ax
	and cx, 0x8000
	shr cx, 15 - 2
	or di, cx

	mov si, dx
	and si, 111110000b
	shr si, 2

	shr ax, 7
	and ax, 11111b

	shr dx, 4
	and dx, 111111100000b
	or ax, dx

	xor dx, dx
	test ax, 1 shl 11
	jz .positive

	or ax, 0xF000
	not dx
.positive:
	ret

	; DI = rs1 addr
	; SI = rs2 addr
	; BX = funct3 off
	; DX:AX = imm
decode_b:
	call decode_s

	and ax, not (1 shl 11)

	test ax, 1
	jz .low_bit_clear

	or ax, 1 shl 11
.low_bit_clear:
	and ax, not 1
	ret

	; DI = rd addr
	; DX:AX = imm
decode_u:
	mov di, ax
	and di, 111110000000b
	shr di, 5
	and ax, 0xF000
	ret

	; DI = rd addr
	; CX:BX = imm
decode_j:
	mov bx, dx
	shr bx, 4
	and bx, 11111111110b

	mov cx, dx
	and cx, 0x8000
	jz .positive

	or cx, 0111111111110000b
.positive:

	mov si, dx
	and si, 10000b
	shl si, 11 - 4
	or bx, si

	mov di, ax
	and di, 111110000000b
	shr di, 5

	and ax, 0xF000
	or bx, ax

	and dx, 0x000F
	or cx, dx

	ret
