virtual at 0x0000
gpr:
	times 32 dw ?, ?
pc dw ?, ?
new_pc dw ?, ?
end virtual


	align 8
gdt:
	dq 0x0000000000000000
	dq 0x000098010000FFFF
repeat 16
	dq 0x000092000000FFFF or (% - 1) shl 32
end repeat
.end:
