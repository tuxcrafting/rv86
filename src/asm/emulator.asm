	; execute the next instruction
step:
	; clear x0
	xor ax, ax
	mov [gpr + 0], ax
	mov [gpr + 2], ax

	; fetch next instruction word
	mov bx, [pc + 0]
	mov cx, [pc + 2]
	call prepare_es
	mov ax, [es:bx + 0]
	mov dx, [es:bx + 2]

	add bx, 4
	adc cx, 0

	; dispatch base opcode
	mov [new_pc + 0], bx
	mov [new_pc + 2], cx

	mov si, ax
	and si, 1111100b
	shr si, 1
	call word [base_opcode_map + si]

	; write back pc
	mov ax, [new_pc + 0]
	mov [pc + 0], ax
	mov ax, [new_pc + 2]
	mov [pc + 2], ax
	ret
