	; set ES to the range of memory in CX (high 16 bits of address)
	; clobbers AX
prepare_es:
	mov ax, cx
	and ax, 0x000F
	shl ax, 3
	add ax, 0x0010
	mov es, ax
	ret

	; DX:AX = offset, SI = base, BX = real offset
	; clobbers AX, CX
prepare_addr:
	mov bx, [si + 0]
	mov cx, [si + 2]
	add bx, ax
	adc cx, dx

	test cx, 0xFFF0
	jnz error

	jmp prepare_es
