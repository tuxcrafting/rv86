	org 0x7C00
	use16

start:
	; ensure our segments are all 0x0000
	xor ax, ax
	mov ds, ax
	mov ss, ax
	jmp 0x0000:.next
.next:

	; load 128 sectors from floppy, this is emulator + "ROM"
	mov ax, 0x1000
	mov es, ax

.retry:
	xor ax, ax
	int 0x13

	mov ax, 0x027F
	mov cx, 0x0001
	mov dh, 0x00
	xor bx, bx
	int 0x13
	jc .retry

	; jump there
	jmp 0x1000:emu_start

gdtr:
	dw gdt.end - gdt - 1
	dd 0x010000 + gdt

times 510 - ($ - $$) db 0x00
	dw 0xAA55
