include "boot.asm"

	org 0x0200

include "data.asm"

emu_start:
	cli
	cld

	; activate A20
	in al, 0x92
	or al, 2
	out 0x92, al

	; enter protected mode
	lgdt [gdtr]
	smsw ax
	or ax, 0001b
	lmsw ax

	jmp 0x0008:.next
.next:
	mov ax, 0x0010 + 0x1 * 8
	mov ds, ax
	mov ax, 0x0010 + 0x0 * 8
	mov ss, ax

	; stack top is 0x010000
	mov sp, 0x0000

	; copy rom to 0x020000
	mov si, rom
	mov ax, 0x0010 + 0x2 * 8
	mov es, ax
	mov di, 0x0000
	mov cx, rom.end - rom
	rep movsb

	mov word [pc + 0], 0x0000
	mov word [pc + 2], 0x0002

.loop:
	call step
	jmp .loop

halt:
	cli
	hlt
	jmp halt

error:
	; print an error message
	mov ax, 0x0010 + 0xB * 8
	mov es, ax
	mov di, 0x8000
	mov si, err_msg
.loop:
	lodsb
	or al, al
	jz .next
	mov ah, 0x4F
	stosw
	jmp .loop
.next:
	mov dx, [pc + 2]
	call print_hex
	mov dx, [pc + 0]
	call print_hex
	jmp halt

err_msg db "error occured at ", 0

	; print 16-bit hex number in DX to ES:DI
	; clobbers AL, BX
print_hex:
repeat 4
	mov bx, dx
	shr bx, 12
	shl dx, 4
	mov al, [bx + alphabet]
	stosw
end repeat
	ret

alphabet db "0123456789ABCDEF"

include "util.asm"
include "address.asm"
include "emulator.asm"
include "decode.asm"
include "operation.asm"

rom:
	file "rv86_rom.bin"
.end:

; end
