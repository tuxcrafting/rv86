#include <stdint.h>

__attribute__((naked))
void _start() {
    __asm__(
        ".option push;\n"
        ".option norelax;\n"
        "1: auipc gp, %pcrel_hi(__global_pointer$);\n"
        "addi gp, gp, %pcrel_lo(1b);\n"
        ".option pop;\n"
        "li sp, 0x00080000;\n"
        "j main_");
}

__attribute__((naked))
void outp(uint16_t port, uint8_t data) {
    __asm__(".long 0x000B; ret");
}

__attribute__((naked))
uint8_t inp(uint16_t port) {
    __asm__(".long 0x800B; ret");
}

#define VRAM ((uint16_t*)0xB8000)
#define WIDTH 80
#define HEIGHT 25

unsigned attr, x, y;

void* memset(void* dest, char c, unsigned n) {
    for (unsigned i = 0; i < n; i++)
        ((char*)dest)[i] = c;
    return dest;
}

void* memcpy(void* dest, const void* src, unsigned n) {
    for (unsigned i = 0; i < n; i++)
        ((char*)dest)[i] = ((const char*)src)[i];
    return dest;
}

void update_cursor() {
    unsigned pos = x + y * WIDTH;
    outp(0x3D4, 0x0F);
    outp(0x3D5, pos & 0xFF);
    outp(0x3D4, 0x0E);
    outp(0x3D5, pos >> 8);
}

void clear() {
    x = y = 0;
    for (unsigned i = 0; i < WIDTH * HEIGHT; i++)
        VRAM[i] = attr;
    update_cursor();
}

void scroll() {
    memcpy(VRAM, &VRAM[WIDTH], WIDTH * (HEIGHT - 1) * 2);
    for (unsigned i = 0; i < WIDTH; i++)
        VRAM[WIDTH * (HEIGHT - 1) + i] = attr;
    if (y > 0) {
        y--;
        update_cursor();
    }
}

void putc(char c) {
    switch (c) {
    case '\n':
        x = 0;
        y++;
        break;
    default:
        VRAM[x + y * WIDTH] = attr | c;
        x++;
    }

    if (x == WIDTH) {
        x = 0;
        y++;
    }
    if (y == HEIGHT)
        scroll();

    update_cursor();
}

void puts(const char* s) {
    for (unsigned i = 0; s[i] != '\0'; i++)
        putc(s[i]);
}

void main_() {
    attr = 0x0700;
    clear();
    puts("RV86 -- Hello, world!\n");
    for (;;);
}
