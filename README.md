# RV86

RV86 is a RISC-V emulator designed to run on an Intel 80286, with the
goal of potentially running modern operating systems such as
Linux.

Well, it's pretty far from that right now, but eh.

## Emulated System

The emulated system uses an RV32I CPU with up to 16 MiB of memory. The
address space is directly mapped to the host address space.

The CPU additionally supports 2 custom instructions:

- 0x0000000B - Output value in `a1` to port in `a0`.
- 0x0000800B - Input value from port in `a0` to `a0`.

## TODO

- Interrupts
- Eventual goal is RV32IMACZicsr with HSU privileged
- Implement SBI in ROM
